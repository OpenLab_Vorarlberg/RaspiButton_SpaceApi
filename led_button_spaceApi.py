import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import time
import requests
import json

PIN_NUMBER = 22
BUTTON_STATE = 0
TIME_SLEEP = 1
AUTHORIZATION_TOKEN = ""

GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(PIN_NUMBER, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
while True: # Run forever
    if GPIO.input(PIN_NUMBER) == GPIO.HIGH and BUTTON_STATE == 0:
        print("Button was pushed!")
        requests.post('https://spaceapi.open-lab.at/update_open_state.php',
        data={"open":"true", "authorization":AUTHORIZATION_TOKEN})
        BUTTON_STATE = 1
        time.sleep(TIME_SLEEP)
        print("awake")
    
    if GPIO.input(PIN_NUMBER) == GPIO.HIGH and BUTTON_STATE == 1:
        print("Button was pushed again")
        BUTTON_STATE = 0
        requests.post('https://spaceapi.open-lab.at/update_open_state.php',
        data={"open":"false", "authorization":AUTHORIZATION_TOKEN})
        time.sleep(TIME_SLEEP)
        print("awake")

